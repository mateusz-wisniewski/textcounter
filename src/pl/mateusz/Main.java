package pl.mateusz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        readFile();
    }

    static void readFile() {
        final String fileName = "resources/text.txt";

        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(fileName));
            String tmp = null;

            int charCounter = 0;
            int whiteCharCounter = 0;
            int wordsCounter = 1;

            while ((tmp = reader.readLine()) != null){
                System.out.println("Napis:\n" + tmp);
                System.out.println("------------------------");

                char[] tab = tmp.toCharArray();

                for (int i = 0; i < tab.length; i++) {
                    if (Character.isWhitespace(tab[i])) {
                        whiteCharCounter++;
                    }
                    if (!Character.isWhitespace(tab[i])) {
                        charCounter++;
                    }
                }

                for (int i = 0; i < tmp.length(); i++) {
                    if (tmp.charAt(i) == ' ') {
                        wordsCounter++;
                    }
                }

                System.out.println("Liczba znaków: " + charCounter);
                System.out.println("Liczba białych znaków: " + whiteCharCounter);
                System.out.println("Liczba słów: " + wordsCounter);
            }
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }finally {
            try{
                reader.close();
            }catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}